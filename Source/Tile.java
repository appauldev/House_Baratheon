
public class Tile implements Comparable<Tile> {
	private int x;
	private int y;
	private Tile cameFrom = null;
	private Double costSoFar = 10000.0;
	private Double priority = 0.0;
	private boolean marked = false;
	private boolean passable;
	private double weight; //determines the cost of going to one tile
	private String terrainType;
	
	//Constructor
	public Tile(int fx, int fy) {
		this.x = fx;
		this.y = fy;
	}
	
	//Getters
	public double getPriority() {
		return priority;
	}
	
	public double getCostSoFar() {
		return costSoFar;
	}
	
	public Tile getCameFrom() {
		return cameFrom;
	}
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public String getTerrainType() {
		return terrainType;
	}
	
	public boolean isPassable() {
		return passable;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public boolean isMarked() {
		return marked;
	}
	
	//Setters
	public void setPriority(double i) {
		priority = i;
	}
	
	public void setCostSoFar(double i) {
		costSoFar = i;
	}
	
	public void setCameFrom(Tile t) {
		cameFrom = t;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setPassable(boolean isPassable) {
		this.passable = isPassable;
	}
	
	public void setMarked(boolean b) {
		marked = b;
	}
	public void setWeight(double w) {
		this.weight = w;
		if(weight == 1)
			terrainType = "ROAD";
		else if(weight == 1.2)
			terrainType = "DIRT";
		else if(weight == 1.3)
			terrainType = "GRASS";
		else if(weight == 1.4)
			terrainType = "MUD";
		else {
			terrainType = "IMPASSABLE";
			passable = false;
		}
	}

	@Override
	public int compareTo(Tile t) {
		return priority.compareTo(t.getPriority());	
	}
	
}
