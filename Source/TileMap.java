import java.util.ArrayList;
import java.util.List;

import com.th.HeroBot;
import com.th.TerrainType;
import com.th.WorldState;

public class TileMap  {
	private int width;
	private int height;
	public int size;
	private ArrayList<Tile> map= new ArrayList<Tile>();
	private Tile[][] mapArray;
	private HeroBot hero;
	
	//Constructor
	public TileMap(HeroBot h) {
		//the worst thing that happened to me are the these two following lines of codes.
		//The previous values are h.getXLocation and h.getYLocation
		width = h.getWorld().getWidth();
		height = h.getWorld().getHeight();
		hero = h;

	}
	
	public void makeMap() {
		WorldState world = hero.getWorld();
		mapArray = new Tile[width][height];
		
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				//Give the coordinates of the tile
				Tile newTile = new Tile(i,j);
				
				//Determine if the tile is passable
				newTile.setPassable(world.isPassable(i, j));
				
				//Determine the terrain type of the tile
				String tileType = world.getTerrain(i, j).toString();
//				System.out.println("Tiletype is a " + tileType);
//				System.out.println(world.isPassable(i, j));
//				
				if(TerrainType.valueOf(tileType).toString() == "ROAD"){
//					System.out.println("This is a " + tileType);
					newTile.setWeight(1);
				}
				else if (TerrainType.valueOf(tileType).toString() == "DIRT") {
//					System.out.println("This is a " + tileType);
					newTile.setWeight(1.2);
				}
				else if (TerrainType.valueOf(tileType).toString() == "GRASS") {
//					System.out.println("This is a " + tileType);
					newTile.setWeight(1.6);
				}
				else if (TerrainType.valueOf(tileType).toString() == ("MUD")) {
//					System.out.println("This is a " + tileType);
					newTile.setWeight(2.2);
				}
				else
//					System.out.println("This is a impassable");
					newTile.setWeight(10);
				
				//Add the tile to the map
				map.add(newTile);
//				System.out.println("Tile added to map");
				mapArray[i][j] = newTile;
//				System.out.println("Tile added to map array");
			}
//				System.out.println("Size of map is " + map.size());
		}
	}
	
	public List<Tile> neighbors(Tile t) {
		List<Tile> neighbah = new ArrayList<Tile>();
		
		if(hero.getWorld().isPassable(t.getX()+1, t.getY())) {
			neighbah.add(getTile(t.getX()+1, t.getY())); //east of the tile
//			if((getTile((t.getX()+1), t.getY()).isPassable()))
//				neighbah.add(getTile(t.getX()+1, t.getY())); //east of the tile
		}
			
		if(hero.getWorld().isPassable(t.getX(), t.getY()+1)) {
			neighbah.add(getTile(t.getX(), t.getY()+1)); //north of the tile
//			if((getTile((t.getX()), t.getY()+1).isPassable())) 
//				neighbah.add(getTile(t.getX(), t.getY()+1)); //north of the tile
		}

		if(hero.getWorld().isPassable(t.getX()-1, t.getY())) {
			neighbah.add(getTile(t.getX()-1, t.getY())); //west of the tile
//			if((getTile((t.getX()+1), t.getY()).isPassable()))
//				neighbah.add(getTile(t.getX()-1, t.getY())); //west of the tile
		}
			
		if(hero.getWorld().isPassable(t.getX(), t.getY()-1)) {
			neighbah.add(getTile(t.getX(), t.getY()-1)); //south of the tile
//			if((getTile((t.getX()), t.getY()-1).isPassable())) 
//				neighbah.add(getTile(t.getX(), t.getY()-1)); //south of the tile	
		}
		
		return neighbah;
	}
	
//	public boolean contains()
	
	//Getters
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public List<Tile> getMap() {
		return map;
	}
	
	public Tile[][] getMapArray() {
		return mapArray;
	}
	
	public Tile getTile(int x, int y) {
		return mapArray[x][y];
	}
	
	public void getSize() {
		System.out.println("Size of map is " + map.size());
	}
	
	//Setters
	public void  setWidth(int w) {
		width = w;
	}
	
	public void setHeight(int h) {
		height = h;
	}
}
