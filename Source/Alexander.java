import com.th.Attackable;
import com.th.Consumable;
import com.th.Equipment;
import com.th.Fighter;
import com.th.HeroBot;
import com.th.Locatable;
import com.th.LocatableType;

import java.util.*;


public class Alexander extends HeroBot {
		boolean run = true;
		private TileMap map;
		
	public void action() {
		
        if (this.getConsumable() != null) {
        	this.useConsumable();
        }
        
        if (this.getEquipment() != null && !this.getEquipStatus()) {
            this.useEquipment(true);
        }
        
        where2Go();//what shall the hero do
        
        while (this.getAvailableStats() > 0) { //apply stat points
        	System.out.println("Enemy terminated. Ours is the fury!");
        	this.applyStatMovement();
        	this.applyStatMovement();
        	this.applyStatAction();
        	this.applyStatDamage();
        	this.applyStatLife();
            this.applyStatDefense();
            this.applyStatAction();
            System.out.println("Life :" + getMaxLife() + " " + getCurrentLife());
            System.out.println("damage: " + getDamage());
            System.out.println("defense: " + getDefense());
            System.out.println("action: " + getActionSpeed());
            System.out.println("action: " + getActionSpeed());
        }
		
	}

	public void beforeAttack(int arg0) {
		this.defend();
	}

	public void clashed(Attackable attacker) {
		attacker.attacked(this.getAttacker());
	}

	public void foundConsumable(Consumable pots) {
		this.pickConsumable(pots);
	}

	public void foundEquipment(Equipment weapon) {
		this.pickEquipment(weapon);
	}
	
	//Alexander's movement
	private void move(int x, int y) {
		if(this.getXLocation() < x) {
			this.moveRight();
		}
		
		else if(this.getXLocation() > x) {
			this.moveLeft();
		}
		
		else if(this.getYLocation() < y) {
			this.moveDown();
		}
		
		else if(this.getYLocation() > y) {
			this.moveUp();
		}
	}

	//Locatable functions
	private Locatable nearestWeapon() {
		return nearest(this.getWorld().getAllEquipments());
	}
	
	private Locatable nearestCreep() {
		return nearest(this.enemyCreepList());
	}
	
	private Locatable nearestPots() {
		return nearest(this.getWorld().getAllConsumables());
	}
	
	private Locatable nearestEnemyHero() {
		return nearest(this.enemyHeroList());
	}
	
	private List<Locatable> enemyHeroList() {
		/* This method is created because getAllEnemyHeroes returns List<Fighter>
		 * What we want is to have List<Locatable> so we have to transfer the contents
		 * of List<Fighter> to a List<Locatable>
		 */
		List<Fighter> fighterList = this.getWorld().getAllEnemyHeroes(this); //get the list of your enemy
		List<Locatable> enmyList = new ArrayList<Locatable>(); //create a list where the fighter list will be stored
	
		enmyList.addAll(fighterList);
		
		return enmyList;
	}
	
	private List<Locatable> enemyCreepList() {
		/* This method is created because getAllEnemyHeroes returns List<Fighter>
		 * What we want is to have List<Locatable> so we have to transfer the contents
		 * of List<Fighter> to a List<Locatable>
		 */
		List<Fighter> fighterList = this.getWorld().getAllCreeps(); //get the list of your enemy
		List<Locatable> enmyList = new ArrayList<Locatable>(); //create a list where the fighter list will be stored
	
		enmyList.addAll(fighterList);
		
		return enmyList;
	}

	private Locatable nearest(List<Locatable> locList) {
		/*
		 * The character hangs if the list returned is empty. Jusko
		 */
		if(locList.isEmpty()) {
			return null;
		}
		/* This method goes over a list and computes the distance between the locatable
		 * and Alexander. After going over the list, this method will return the nearest 
		 * locatable to our hero.
		 */
		
		/*The index of the locatable must be save, so create a variable for this
			natutunan ko na hindi pala pwedeng ireturn ang isang variable kapag hindi initialized
		*/
		int retIndex = 0;
		
		/*Set a number for the current distance. This will be compared to the evaluated locatable in the for-loop
		 * Note na kahit anong number pwede dito as long as high enough or else kapag sobrang baba nung number
		 * baka ito na lang yung ireturn nitong method
		*/	 
		double currNearest = 100000d;
		int len = locList.size(); //size of the list, para sa condition ng for-loop
		
		for (int i = 0; i < len; i++) {
			/*
			 * The code readability versus performance dilemma.
			 */
			double distanceI = heuristic(locList.get(i).getXLocation(), locList.get(i).getYLocation());
			if (distanceI < currNearest) {
				currNearest = distanceI; //make the computer distance the nearest distance
				retIndex = i; // Save the index of the current nearest locatable
				continue;
			}
		}
		
		/*
		 * use retIndex to return the locatable item from the list
		 */
		return locList.get(retIndex);
	}
	
	
	///Pathfinding Functions
	private double heuristic(int xLocation, int yLocation) {
		int x = Math.abs(this.getXLocation()- xLocation);
		int y = Math.abs(this.getXLocation() - yLocation);
		
		return (double)(x+y);
	}

	private double heuristic(Tile current, Locatable goal) { //Manhattan Distance
		int x = Math.abs(current.getX()- goal.getXLocation());
		int y = Math.abs(current.getY() - goal.getYLocation());
		
		return (double)(x+y);
	}
	
	private Tile breadthSearch(Locatable goal) { //we tried implementing A* but the result is more of a Breadthsearch
		map = new TileMap(this);
		map.makeMap();
		
		boolean goalReached = false;
		LinkedList<Tile> frontier = new LinkedList<Tile>();
		double prevTileCost, newTileCost, priority;
//		Tile heroLoc = map.getTile(getXLocation(), getYLocation());
		Tile heroLoc = new Tile(getXLocation(), getYLocation());
		frontier.add(heroLoc);
		Tile current = new Tile(0,0);
		
		while(!goalReached) {
			//Sort the list so that the tile with the lowest cost will be the first to be removed
			Collections.sort(frontier);  
			current = frontier.remove();
//			System.out.println("Current is " + current.getX() +" "+ current.getY());
			
			if(current.getX() == goal.getXLocation() & current.getY() == goal.getYLocation()) {
				goalReached = true;
				break;
			}
			else {
				for (Tile tile : map.neighbors(current)) {
					prevTileCost = tile.getCostSoFar();
					newTileCost = current.getCostSoFar()+tile.getWeight();
					
					if((!tile.isMarked()) | newTileCost < prevTileCost) {
						tile.setCostSoFar(newTileCost);
						frontier.add(tile);
						priority = newTileCost + heuristic(tile, goal);
						tile.setPriority(priority);
						frontier.add(tile);
						tile.setCameFrom(current);
						tile.setMarked(true);
					}
				}
			}
		}
		
		return current; //return the tile which contains the path
	}
	
	private void travelPath(Tile t) {
		ArrayList<Tile> path = new ArrayList<Tile>();
		
		//Since the path is reversed (the goal tile is returned), we need to reverse the path
		while(t.getCameFrom() != null) {
//			System.out.print(t.getX() + " " + t.getY());
			path.add(0, t);
			t = t.getCameFrom();
		}
		
		for (Tile tile : path) {
//			System.out.println("moving to " + tile.getX()+" "+ tile.getY());
			move(tile.getX(), tile.getY()); //move to each tile
		}
	}
	
	private void where2Go() { //determine how the player will move
		Locatable a = nearestPots();
		Locatable b = nearestWeapon();
		Locatable c = nearestCreep();
		Locatable d = nearestEnemyHero();
		
		if(a != null) {
			travelPath(breadthSearch(a));
		}
			
		else if(b != null) {
			travelPath(breadthSearch(b));
		}
			
		else if(c != null) {
			if(this.getLife() > 0.50) {
				travelPath(breadthSearch(c));
			}
		}
		
		else if(d != null) {
			if(this.getLife() > 0.75) {
				travelPath(breadthSearch(d));
			}
		}
	}
	


}
